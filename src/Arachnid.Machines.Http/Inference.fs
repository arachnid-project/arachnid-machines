﻿namespace Arachnid.Machines.Http

open System
open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Machines
open Arachnid.Types.Http
open Arachnid.Types.Language
open Hephaestus

(* Inference

   Inference modules and functions for commonly inferred types, when not
   defined elsewhere. *)

[<AutoOpen>]
module Inference =

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module Charsets =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member Charsets (x: Arachnid<Charset list>) =
                    Dynamic (Set.ofList <!> x)

                static member Charsets (x: Charset list) =
                    Static (Set.ofList x)

                static member Charsets (x: Charset) =
                    Static (Set.singleton x)

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member Charsets: ^a -> Value<Set<Charset>>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module Components =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member Components (x: Set<Component<Configuration,unit,State>> list) =
                    Set.unionMany x

                static member Components (x: Set<Component<Configuration,unit,State>>) =
                    x

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member Components: ^a -> Set<Component<Configuration,unit,State>>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module ContentCodings =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member ContentCodings (x: Arachnid<ContentCoding list>) =
                    Dynamic (Set.ofList <!> x)

                static member ContentCodings (x: ContentCoding list) =
                    Static (Set.ofList x)

                static member ContentCodings (x: ContentCoding) =
                    Static (Set.singleton x)

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member ContentCodings: ^a -> Value<Set<ContentCoding>>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module DateTime =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member DateTime (x: Arachnid<DateTime>) =
                    Dynamic x

                static member DateTime (x: DateTime) =
                    Static x

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member DateTime: ^a -> Value<DateTime>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module Decision =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member Decision (x: Arachnid<bool>) =
                    Dynamic x

                static member Decision (x: bool) =
                    Static x

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member Decision: ^a -> Value<bool>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module EntityTag =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member ETag (x: Arachnid<EntityTag>) =
                    Dynamic x

                static member ETag (x: EntityTag) =
                    Static x

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member ETag: ^a -> Value<EntityTag>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module Handler =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member inline Handler (x: Acceptable -> Arachnid<Representation>) =
                    x

                static member inline Handler (x: Arachnid<Representation>) =
                    fun (_: Acceptable) -> x

                static member inline Handler (x: Representation) =
                    fun (_: Acceptable) -> Arachnid.init x

            let inline defaults (a: ^a, _: ^b) =
                    ((^a or ^b) : (static member Handler: ^a -> (Acceptable -> Arachnid<Representation>)) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module LanguageTags =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member LanguageTags (x: Arachnid<LanguageTag list>) =
                    Dynamic (Set.ofList <!> x)

                static member LanguageTags (x: LanguageTag list) =
                    Static (Set.ofList x)

                static member LanguageTags (x: LanguageTag) =
                    Static (Set.singleton x)

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member LanguageTags: ^a -> Value<Set<LanguageTag>>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module MediaTypes =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member MediaTypes (x: Arachnid<MediaType list>) =
                    Dynamic (Set.ofList <!> x)

                static member MediaTypes (x: MediaType list) =
                    Static (Set.ofList x)

                static member MediaTypes (x: MediaType) =
                    Static (Set.singleton x)

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member MediaTypes: ^a -> Value<Set<MediaType>>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module Methods =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            type Defaults =
                | Defaults

                static member Methods (x: Arachnid<Method list>) =
                    Dynamic (Set.ofList <!> x)

                static member Methods (x: Method list) =
                    Static (Set.ofList x)

                static member Methods (x: Method) =
                    Static (Set.singleton x)

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member Methods: ^a -> Value<Set<Method>>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v

    [<RequireQualifiedAccess>]
    [<CompilationRepresentation (CompilationRepresentationFlags.ModuleSuffix)>]
    module Operation =

        (* Inference *)

        [<RequireQualifiedAccess>]
        module Inference =

            let private alwaysTrue : (unit -> bool) = (do ()); fun () -> true

            type Defaults =
                | Defaults

                static member Operation (x: Arachnid<bool>) =
                    x

                static member Operation (x: Arachnid<unit>) =
                    x |> Arachnid.map alwaysTrue

            let inline defaults (a: ^a, _: ^b) =
                ((^a or ^b) : (static member Operation: ^a -> Arachnid<bool>) a)

            let inline infer (x: 'a) =
                defaults (x, Defaults)

        let inline infer v =
            Inference.infer v
