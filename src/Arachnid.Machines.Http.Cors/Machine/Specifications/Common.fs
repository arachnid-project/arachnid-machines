﻿namespace Arachnid.Machines.Http.Cors.Machine.Specifications

open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Core.Optics
open Arachnid.Machines
open Arachnid.Machines.Http.Cors
open Arachnid.Machines.Http.Cors.Machine.Configuration
open Arachnid.Optics.Http.Cors
open Arachnid.Types.Http.Cors

(* Common *)

[<RequireQualifiedAccess>]
module Common =

    (* Aliases

       Shorthand/abbreviations for common functions, used locally to make the
       code more concise where these verbose formulations make the logic harder
       to read. *)

    (* Monadic *)

    let private lift =
        Arachnid.Value.lift

    (* Optics *)

    let private origins_ =
        Properties.Resource.origins_

    (* Decisions *)

    [<RequireQualifiedAccess>]
    module Decisions =

        (* Enabled *)

        let enabled k =
            Decision.create (k, "enabled")
                (function | TryGetOrElse Extension.enabled_ (Static true) x -> x)

        (* Origin *)

        let hasOrigin k =
            Decision.create (k, "has-origin")
                (function | _ -> Dynamic (Option.isSome <!> !. Request.Headers.origin_))

        let rec originAllowed k =
            Decision.create (k, "origin-allowed")
                (function | Get origins_ None -> Static true
                          | TryGet origins_ (Static x) when Set.isEmpty x -> Static false
                          | TryGet origins_ x -> Dynamic (allow <!> lift x <*> !. Request.Headers.origin_)
                          | _ -> Static false)

        and private allow origins =
            function | Some (Origin (OriginListOrNull.Origins [ x ])) when Set.contains x origins -> true
                     | _ -> false

    (* Support *)

    let internal allowHeaders =
            Arachnid.Optic.get Request.Headers.accessControlRequestHeaders_
        >>= Operations.allowHeaders

    let internal allowMethods =
            Arachnid.Optic.get Request.Headers.accessControlRequestMethod_
        >>= Operations.allowMethods

    let internal allowOriginAndSupportsCredentials supportsCredentials origins =
            Arachnid.Optic.get Request.Headers.origin_
        >>= fun origin ->
            Arachnid.Value.lift supportsCredentials
        >>= fun supportsCredentials ->
            Arachnid.Value.liftOption origins
        >>= fun origins ->
            Operations.allowOriginAndSupportsCredentials origin supportsCredentials origins

    let internal exposeHeaders exposedHeaders =
            Arachnid.Value.liftOption exposedHeaders
        >>= Operations.exposeHeaders

    let internal maxAge maxAge =
            Arachnid.Value.liftOption maxAge
        >>= Operations.maxAge
