﻿namespace Arachnid.Machines.Http.Patch.Machine.Specifications

open Arachnid.Machines

(* Main *)

[<RequireQualifiedAccess>]
module Main =

    (* Key *)

    let private key =
        Key.root >> Key.add [ "main" ]

    (* Decisions *)

    [<RequireQualifiedAccess>]
    module Decisions =

        (* Enabled *)

        let enabled k =
            Common.Decisions.enabled (key k)

    let specification =
        Decisions.enabled
