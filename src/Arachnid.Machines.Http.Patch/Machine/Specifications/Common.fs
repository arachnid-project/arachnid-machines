﻿namespace Arachnid.Machines.Http.Patch.Machine.Specifications

open Arachnid.Core.Operators
open Arachnid.Machines
open Arachnid.Machines.Http.Patch
open Arachnid.Machines.Http.Patch.Machine.Configuration

(* Common *)

[<RequireQualifiedAccess>]
module Common =

    (* Decisions *)

    [<RequireQualifiedAccess>]
    module Decisions =

        (* Enabled *)

        let enabled k =
            Decision.create (k, "enabled")
                (function | TryGetOrElse Extension.enabled_ (Static true) x -> x)

    (* Support *)

    let internal acceptPatch mediaTypes =
            Arachnid.Value.lift mediaTypes
        >>= Operations.acceptPatch
