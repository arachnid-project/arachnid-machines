﻿namespace Arachnid.Machines.Http.Patch

open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Optics.Http.Patch
open Arachnid.Types.Http.Patch

(* Operations *)

[<RequireQualifiedAccess>]
module Operations =

    (* Operations

       Operations for setting PATCH related response headers, including logic
       to determine permutation when the input data is potentially of varying
       shapes, and may have dependent logic. *)

    (* Accept Patch *)

    let rec acceptPatch mediaTypes =
        acceptPatchPermutations mediaTypes

    and private acceptPatchPermutations =
        function | x when Set.isEmpty x -> Arachnid.empty 
                 | x -> Response.Headers.acceptPatch_ .= Some (AcceptPatch (Set.toList x))
