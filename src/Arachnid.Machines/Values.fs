﻿namespace Arachnid.Machines

open Arachnid.Core
open Arachnid.Core.Operators
open Hephaestus

(* Types

   Types representing some core concepts within the design and configuration
   of a conceptual Machine for the Arachnid system.

   The concept of a Decision maps the core Hephaestus abstraction of a
   decision of left or right to Arachnid, using simple boolean true or false to
   represent branching points (left or right are always intended to be an
   abstraction which should be targeted by a domain specific abstraction.

   The concept of Configuration represents an extensible container for settings
   of any kind, which may be populated by any relevant component within the
   system to provide a source of setting information which cannot be known at
   design time.

   Optics are provided for these core Machine types. *)

type Value<'a> =
    | Dynamic of Arachnid<'a>
    | Static of 'a

    static member dynamic_ =
        (function | Dynamic f -> Some f
                  | _ -> None), (Dynamic)

    static member static_ =
        (function | Static l -> Some l
                  | _ -> None), (Static)

(* Value

   Functions for working with Arachnid values, binding a Arachnid value (function)
   inside a Value, mapping the dynamic or static Value function to the Arachnid
   function. *)

[<RequireQualifiedAccess>]
module Value =

    [<RequireQualifiedAccess>]
    module Arachnid =

        let bind f =
            function | Dynamic x -> Dynamic (f =<< x)
                     | Static x -> Dynamic (f x)

        let map f =
            function | Dynamic x -> Dynamic (f <!> x)
                     | Static x -> Static (f x)

(* Arachnid

   Functions for working with Values, applying them to Arachnid functions, and
   lifting the contained values to Arachnid functions where required (for both
   simple and optional values). *)

[<RequireQualifiedAccess>]
module Arachnid =

    [<RequireQualifiedAccess>]
    module Value =

        let apply f =
            function | Dynamic x -> f =<< x
                     | Static x -> f x

        let lift =
            function | Dynamic x -> x
                     | Static x -> Arachnid.init x

        let liftOption =
            function | Some v -> Some <!> lift v
                     | _ -> Arachnid.init None

(* Decisions

   Simple mapping from a Arachnid Decision to a stateful Hephaestus Decision,
   parameterized by the Arachnid State type. *)

[<RequireQualifiedAccess>]
module Decision =

    let private convert =
        function | true -> Right
                 | _ -> Left

    let map =
        function | Dynamic f -> Function (convert <!> f) 
                 | Static l -> Literal (convert l)
