# Arachnid Machines

## Overview

Arachnid Machines provide strongly type high-level abstractions over common web logic requirements. In particular, the Arachnid HTTP Machine provides a powerful way to model and deploy HTTP resources, adhering to web standards closely and providing reasonable assurances of functional safety and correctness.

## Status

[![pipeline status](https://gitlab.com/arachnid-project/arachnid-machines/badges/master/pipeline.svg)](https://gitlab.com/arachnid-project/arachnid-machines/commits/master)

## See Also

For more information see the [meta-repository for the Arachnid Web Stack](https://gitlab.com/arachnid-project/arachnid), along with the main [arachnid.io](https://arachnid.io) site.
