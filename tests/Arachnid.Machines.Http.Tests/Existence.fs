﻿module Arachnid.Machines.Http.Tests.Existence

open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Testing
open Arachnid.Testing.Operators
open Xunit

(* Existence

   Verification that the Existence specification behaves as expected given
   suitable input. *)

(* Exists *)

[<Fact>]
let ``machine handles exists decision correctly`` () =

    (* Static *)

    let staticMachine =
        arachnidHttpMachine {
            exists true }

    verify defaultSetup staticMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

    (* Dynamic *)

    let setup =
        Request.path_ .= "/exists"

    let dynamicMachine =
        arachnidHttpMachine {
            exists ((=) "/exists" <!> !. Request.path_) }

    verify setup dynamicMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

    verify defaultSetup dynamicMachine [
        Response.statusCode_ => Some 404
        Response.reasonPhrase_ => Some "Not Found" ]
