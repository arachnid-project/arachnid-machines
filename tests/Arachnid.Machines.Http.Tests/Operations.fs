﻿module Arachnid.Machines.Http.Tests.Operation

open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Testing
open Arachnid.Testing.Operators
open Arachnid.Types.Http
open Xunit

(* Operation

   Verification that the Operation element behaves as expected given suitable
   input, including verification that the operation to be run is executed and
   that the result of the operation influences the response path. *)

(* Operation *)

[<Fact>]
let ``machine processes operation correctly`` () =

    let setup =
            (Request.method_ .= POST)
         *> (Request.Headers.contentLength_ .= Some (ContentLength 0))

    (* Success *)

    let successMachine =
        arachnidHttpMachine {
            doPost (defaultValue .= Some "Success")
            methods POST }

    verify setup successMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK"
        defaultValue => Some "Success" ]

    (* Failure *)

    let failureMachine =
        arachnidHttpMachine {
            doPost ((defaultValue .= Some "Failure") *> (Arachnid.init false))
            methods POST }

    verify setup failureMachine [
        Response.statusCode_ => Some 500
        Response.reasonPhrase_ => Some "Internal Server Error"
        defaultValue => Some "Failure" ]

(* Completed *)

[<Fact>]
let ``machine handles completed correctly`` () =

    let setup =
            (Request.method_ .= POST)
         *> (Request.Headers.contentLength_ .= Some (ContentLength 0))

    (* Completed *)

    let completedMachine =
        arachnidHttpMachine {
            methods POST }

    verify setup completedMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

    (* Uncompleted *)

    let uncompletedMachine =
        arachnidHttpMachine {
            completed false
            methods POST }

    verify setup uncompletedMachine [
        Response.statusCode_ => Some 202
        Response.reasonPhrase_ => Some "Accepted" ]
