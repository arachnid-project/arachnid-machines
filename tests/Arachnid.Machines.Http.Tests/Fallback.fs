﻿module Arachnid.Machines.Http.Tests.Fallback

open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Testing
open Arachnid.Testing.Operators
open Arachnid.Types.Http
open Xunit

(* Fallback

   Verification that the Fallback specification behaves correctly given
   appropriate input. *)

[<Fact>]
let ``machine invokes handleFallback correctly`` () =

    (* Fallback *)

    let FOO =
        Method.Custom "FOO"

    let setup =
        Request.method_ .= FOO

    let machine =
        arachnidHttpMachine {
            methods FOO
            handleFallback ((defaultValue .= Some "Fallback") *> defaultRepresentation) }

    verify setup machine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK"
        defaultValue => Some "Fallback" ]
