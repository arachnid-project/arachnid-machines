﻿module Arachnid.Machines.Http.Tests.Validations

open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Testing
open Arachnid.Testing.Operators
open Arachnid.Types.Http
open Xunit

(* Validations

   Verification that the Validations specifications behaves as expected given
   suitable input. *)

(* Expectation Met *)

[<Fact>]
let ``machine handles expectationMet correctly`` () =

    (* Static *)

    let staticMachine =
        arachnidHttpMachine {
            expectationMet false }

    verify defaultSetup staticMachine [
        Response.statusCode_ => Some 417
        Response.reasonPhrase_ => Some "Expectation Failed" ]

    (* Dynamic *)

    let setup =
        Request.path_ .= "/unmet"

    let dynamicMachine =
        arachnidHttpMachine {
            expectationMet ((<>) "/unmet" <!> !. Request.path_) }

    verify setup dynamicMachine [
        Response.statusCode_ => Some 417
        Response.reasonPhrase_ => Some "Expectation Failed" ]

    verify defaultSetup dynamicMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

(* Method Allowed *)

[<Fact>]
let ``machine handles method allowance correctly`` () =

    (* Static *)

    let staticMachine =
        arachnidHttpMachine {
            methods POST }

    verify defaultSetup staticMachine [
        Response.statusCode_ => Some 405
        Response.reasonPhrase_ => Some "Method Not Allowed"
        Response.Headers.allow_ => Some (Allow [ POST ]) ]

(* URI Too Long *)

[<Fact>]
let ``machine handles uriTooLong correctly`` () =

    (* Static *)

    let staticMachine =
        arachnidHttpMachine {
            uriTooLong true }

    verify defaultSetup staticMachine [
        Response.statusCode_ => Some 414
        Response.reasonPhrase_ => Some "URI Too Long" ]

    (* Dynamic *)

    let setup =
        Request.path_ .= "/uritoolong"

    let dynamicMachine =
        arachnidHttpMachine {
            uriTooLong ((=) "/uritoolong" <!> !. Request.path_) }

    verify setup dynamicMachine [
        Response.statusCode_ => Some 414
        Response.reasonPhrase_ => Some "URI Too Long" ]

    verify defaultSetup dynamicMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

(* Bad Request *)

[<Fact>]
let ``machine handles badRequest correctly`` () =

    (* Static *)

    let staticMachine =
        arachnidHttpMachine {
            badRequest true }

    verify defaultSetup staticMachine [
        Response.statusCode_ => Some 400
        Response.reasonPhrase_ => Some "Bad Request" ]

    (* Dynamic *)

    let setup =
        Request.path_ .= "/badrequest"

    let dynamicMachine =
        arachnidHttpMachine {
            badRequest ((=) "/badrequest" <!> !. Request.path_) }

    verify setup dynamicMachine [
        Response.statusCode_ => Some 400
        Response.reasonPhrase_ => Some "Bad Request" ]

    verify defaultSetup dynamicMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]
