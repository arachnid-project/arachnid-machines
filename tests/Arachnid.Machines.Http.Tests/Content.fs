﻿module Arachnid.Machines.Http.Tests.Content

open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Testing
open Arachnid.Testing.Operators
open Arachnid.Types.Http
open Xunit

(* Content

   Verification that the Content element behaves as expected given suitable
   input. *)

(* Content-Length *)

[<Fact>]
let ``machine handles absent content length correctly`` () =

    let setup =
            (Request.method_ .= POST)

    (* Content-Length *)

    let machine =
        arachnidHttpMachine {
            methods POST }

    verify setup machine [
        Response.statusCode_ => Some 411
        Response.reasonPhrase_ => Some "Length Required" ]

(* Content-Type *)

[<Fact>]
let ``machine handles unsupported content type correctly`` () =

    let setup =
            (Request.method_ .= POST)
         *> (Request.Headers.contentLength_ .= Some (ContentLength 0))
         *> (Request.Headers.contentType_ .= Some (ContentType (MediaType.Text)))

    (* Content-Length *)

    let machine =
        arachnidHttpMachine {
            acceptableMediaTypes MediaType.Json
            methods POST }

    verify setup machine [
        Response.statusCode_ => Some 415
        Response.reasonPhrase_ => Some "Unsupported Media Type" ]
