﻿module Arachnid.Machines.Http.Tests.Permissions

open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Testing
open Arachnid.Testing.Operators
open Xunit

(* Permission

   Verification that the Permission element behaves as expected given suitable
   input. *)

(* Authorized *)

[<Fact>]
let ``machine handles authorized correctly`` () =

    (* Static *)

    let staticMachine =
        arachnidHttpMachine {
            authorized false }

    verify defaultSetup staticMachine [
        Response.statusCode_ => Some 401
        Response.reasonPhrase_ => Some "Unauthorized" ]

    (* Dynamic *)

    let setup =
        Request.path_ .= "/authorized"

    let dynamicMachine =
        arachnidHttpMachine {
            authorized ((=) "/authorized" <!> !. Request.path_) }

    verify setup dynamicMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

    verify defaultSetup dynamicMachine [
        Response.statusCode_ => Some 401
        Response.reasonPhrase_ => Some "Unauthorized" ]

(* Allowed *)

[<Fact>]
let ``machine handles allowed correctly`` () =

    (* Static *)

    let staticMachine =
        arachnidHttpMachine {
            allowed false }

    verify defaultSetup staticMachine [
        Response.statusCode_ => Some 403
        Response.reasonPhrase_ => Some "Forbidden" ]

    (* Dynamic *)

    let setup =
        Request.path_ .= "/allowed"

    let dynamicMachine =
        arachnidHttpMachine {
            allowed ((=) "/allowed" <!> !. Request.path_) }

    verify setup dynamicMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

    verify defaultSetup dynamicMachine [
        Response.statusCode_ => Some 403
        Response.reasonPhrase_ => Some "Forbidden" ]
