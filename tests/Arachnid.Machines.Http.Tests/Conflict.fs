﻿module Arachnid.Machines.Http.Tests.Conflict

open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Testing
open Arachnid.Testing.Operators
open Arachnid.Types.Http
open Xunit

(* Conflict

   Verification that the Conflict element behaves as expected given suitable
   input. *)

(* Conflict *)

[<Fact>]
let ``machine handles conflict correctly`` () =

    let nonConflictSetup =
            (Request.method_ .= POST)
         *> (Request.Headers.contentLength_ .= Some (ContentLength 0))

    let conflictSetup =
            (Request.path_ .= "/conflict")
         *> (Request.method_ .= POST)
         *> (Request.Headers.contentLength_ .= Some (ContentLength 0))

    (* Static *)

    let staticMachine =
        arachnidHttpMachine {
            conflict true
            methods POST }

    verify nonConflictSetup staticMachine [
        Response.statusCode_ => Some 409
        Response.reasonPhrase_ => Some "Conflict" ]

    (* Dynamic *)

    let dynamicMachine =
        arachnidHttpMachine {
            conflict ((=) "/conflict" <!> !. Request.path_)
            methods POST }

    verify nonConflictSetup dynamicMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

    verify conflictSetup dynamicMachine [
        Response.statusCode_ => Some 409
        Response.reasonPhrase_ => Some "Conflict" ]
