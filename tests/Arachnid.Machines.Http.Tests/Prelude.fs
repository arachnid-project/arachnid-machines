﻿[<AutoOpen>]
module Arachnid.Machines.Http.Tests.Prelude

open Aether
open Arachnid.Core
open Arachnid.Machines.Http

(* Optics *)

let defaultValue : Lens<State,string option> =
    State.value_ "default"

(* Fixtures *)

let defaultSetup =
    Arachnid.empty

let defaultRepresentation =
    Arachnid.init Representation.empty

(* Machines *)

let defaultMachine =
    arachnidHttpMachine {
        return () }
