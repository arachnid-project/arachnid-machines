﻿module Arachnid.Machines.Http.Cors.Tests.Simple

open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Machines.Http.Cors
open Arachnid.Optics.Http.Cors
open Arachnid.Testing
open Arachnid.Testing.Operators
open Arachnid.Types.Http.Cors
open Xunit

(* Defaults *)

let defaultSetup =
        (Request.Headers.origin_ .= Some (Origin (OriginListOrNull.Origins [ Xyncro.com ])))

(* Simple

   Verification that CORS Simple requests are handled correctly given
   appropriate request data. *)

[<Fact>]
let ``basic cors allows request correctly`` () =

    let machine =
        arachnidHttpMachine {
            cors }

    verify defaultSetup machine [
        Response.Headers.accessControlAllowOrigin_ => Some (AccessControlAllowOrigin (Origins (OriginListOrNull.Origins [ Xyncro.com ])))
        Response.Headers.accessControlAllowCredentials_ => Some (AccessControlAllowCredentials) ]

[<Fact>]
let ``cors disabled ignores cors`` () =

    let machine =
        arachnidHttpMachine {
            cors
            corsEnabled false }

    verify defaultSetup machine [
        Response.Headers.accessControlAllowOrigin_ => None
        Response.Headers.accessControlAllowCredentials_ => None ]

[<Fact>]
let ``credentials unsupported cors behaves correctly`` () =

    (* Credentials Unsupported, Origins Unspecified *)

    let machine =
        arachnidHttpMachine {
            cors
            corsSupportsCredentials false }

    verify defaultSetup machine [
        Response.Headers.accessControlAllowOrigin_ => Some (AccessControlAllowOrigin (Any))
        Response.Headers.accessControlAllowCredentials_ => None ]

    (* Credentials Unsupported, Origins Empty *)

    let machine =
        arachnidHttpMachine {
            cors
            corsOrigins []
            corsSupportsCredentials false }

    verify defaultSetup machine [
        Response.Headers.accessControlAllowOrigin_ => None
        Response.Headers.accessControlAllowCredentials_ => None ]

    (* Credentials Unsupported, Origins Restricted *)

    let machine =
        arachnidHttpMachine {
            cors
            corsOrigins [ Xyncro.com ]
            corsSupportsCredentials false }

    verify defaultSetup machine [
        Response.Headers.accessControlAllowOrigin_ => Some (AccessControlAllowOrigin (Origins (OriginListOrNull.Origins [ Xyncro.com ])))
        Response.Headers.accessControlAllowCredentials_ => None ]

[<Fact>]
let ``headers exposed behaves correctly`` () =

    (* Exposed Headers Unspecified *)

    let machine =
        arachnidHttpMachine {
            cors }

    verify defaultSetup machine [
        Response.Headers.accessControlExposeHeaders_ => None ]

    (* Exposed Headers Empty *)

    let machine =
        arachnidHttpMachine {
            cors
            corsExposedHeaders [] }

    verify defaultSetup machine [
        Response.Headers.accessControlExposeHeaders_ => None ]

    (* Exposed Headers Non-Empty *)

    let machine =
        arachnidHttpMachine {
            cors
            corsExposedHeaders [ "Server" ] }

    verify defaultSetup machine [
        Response.Headers.accessControlExposeHeaders_ => Some (AccessControlExposeHeaders ([ "Server" ])) ]
