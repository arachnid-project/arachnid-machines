﻿module Arachnid.Machines.Http.Cors.Tests.Preflight

open System
open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Machines.Http.Cors
open Arachnid.Optics.Http
open Arachnid.Optics.Http.Cors
open Arachnid.Testing
open Arachnid.Testing.Operators
open Arachnid.Types.Http
open Arachnid.Types.Http.Cors
open Xunit

(* Defaults *)

let defaultSetup =
        (Request.method_ .= OPTIONS)
     *> (Request.Headers.accessControlRequestMethod_ .= Some (AccessControlRequestMethod GET))
     *> (Request.Headers.origin_ .= Some (Origin (OriginListOrNull.Origins [ Xyncro.com ])))

(* Preflight

   Verification that CORS Preflight requests are handled correctly when given
   appropriate request data. *)

[<Fact>]
let ``basic cors allows request correctly`` () =

    let machine =
        arachnidHttpMachine {
            cors }

    let setup =
            (defaultSetup)
         *> (Request.Headers.accessControlRequestHeaders_ .= Some (AccessControlRequestHeaders [ "Server" ]))

    verify setup machine [
        Response.Headers.accessControlAllowCredentials_ => Some (AccessControlAllowCredentials)
        Response.Headers.accessControlAllowHeaders_ => Some (AccessControlAllowHeaders [ "Server" ])
        Response.Headers.accessControlAllowOrigin_ => Some (AccessControlAllowOrigin (Origins (OriginListOrNull.Origins [ Xyncro.com ])))
        Response.Headers.accessControlAllowMethods_ => Some (AccessControlAllowMethods [ GET ])
        Response.Headers.accessControlMaxAge_ => None ]

[<Fact>]
let ``cors disabled ignores cors`` () =

    let machine =
        arachnidHttpMachine {
            cors
            corsEnabled false }

    verify defaultSetup machine [
        Response.Headers.accessControlAllowOrigin_ => None
        Response.Headers.accessControlAllowCredentials_ => None ]

[<Fact>]
let ``credentials unsupported cors behaves correctly`` () =

    (* Credentials Unsupported, Origins Unspecified *)

    let machine =
        arachnidHttpMachine {
            cors
            corsSupportsCredentials false }

    verify defaultSetup machine [
        Response.Headers.accessControlAllowOrigin_ => Some (AccessControlAllowOrigin (Any))
        Response.Headers.accessControlAllowCredentials_ => None ]

    (* Credentials Unsupported, Origins Empty *)

    let machine =
        arachnidHttpMachine {
            cors
            corsOrigins []
            corsSupportsCredentials false }

    verify defaultSetup machine [
        Response.Headers.accessControlAllowOrigin_ => None
        Response.Headers.accessControlAllowCredentials_ => None ]

    (* Credentials Unsupported, Origins Restricted *)

    let machine =
        arachnidHttpMachine {
            cors
            corsOrigins [ Xyncro.com ]
            corsSupportsCredentials false }

    verify defaultSetup machine [
        Response.Headers.accessControlAllowOrigin_ => Some (AccessControlAllowOrigin (Origins (OriginListOrNull.Origins [ Xyncro.com ])))
        Response.Headers.accessControlAllowCredentials_ => None ]

[<Fact>]
let ``headers behaves correctly`` () =

    let setup =
            (defaultSetup)
         *> (Request.Headers.accessControlRequestHeaders_ .= Some (AccessControlRequestHeaders [ "Server" ]))

    (* Exposed Headers Unspecified *)

    let machine =
        arachnidHttpMachine {
            cors }

    verify setup machine [
        Response.Headers.accessControlAllowHeaders_ => Some (AccessControlAllowHeaders ([ "Server" ])) ]

    (* Exposed Headers Empty *)

    let machine =
        arachnidHttpMachine {
            cors
            corsHeaders [] }

    verify setup machine [
        Response.Headers.accessControlAllowHeaders_ => None ]

    (* Exposed Headers Non-Empty *)

    let machine =
        arachnidHttpMachine {
            cors
            corsHeaders [ "Server" ] }

    verify setup machine [
        Response.Headers.accessControlAllowHeaders_ => Some (AccessControlAllowHeaders ([ "Server" ])) ]

[<Fact>]
let ``max age behaves correctly`` () =

    (* No Max Age *)

    let machine =
        arachnidHttpMachine {
            cors }

    verify defaultSetup machine [
        Response.Headers.accessControlMaxAge_ => None ]

    (* Max Age*)

    let machine =
        arachnidHttpMachine {
            cors
            corsMaxAge 3600 }

    verify defaultSetup machine [
        Response.Headers.accessControlMaxAge_ => Some (AccessControlMaxAge (TimeSpan.FromSeconds 3600.)) ]
