﻿[<AutoOpen>]
module Arachnid.Machines.Http.Cors.Tests.Prelude

open Arachnid.Types.Http.Cors
open Arachnid.Types.Uri

(* Fixtures *)

[<RequireQualifiedAccess>]
module Xyncro =

    let com =
        SerializedOrigin (
            Scheme "http",
            Name (RegName "xyncro.com"),
            None)
