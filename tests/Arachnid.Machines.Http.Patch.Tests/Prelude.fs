﻿[<AutoOpen>]
module Arachnid.Machines.Http.Patch.Tests.Prelude

open Aether
open Arachnid.Core

(* Optics *)

let defaultValue : Lens<State,string option> =
    State.value_ "default"
