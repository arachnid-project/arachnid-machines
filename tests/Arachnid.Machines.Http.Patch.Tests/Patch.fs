﻿module Arachnid.Machines.Http.Patch.Tests.Patch

open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Machines.Http.Patch
open Arachnid.Optics.Http
open Arachnid.Optics.Http.Patch
open Arachnid.Testing
open Arachnid.Testing.Operators
open Arachnid.Types.Http
open Arachnid.Types.Http.Patch
open Xunit

(* Defaults *)

let defaultSetup =
        (Request.method_ .= PATCH)
     *> (Request.Headers.contentLength_ .= Some (ContentLength 0))

(* Main

   Verification that PATCH support behaves broadly correctly. *)

[<Fact>]
let ``patch allows request correctly`` () =

    let machine =
        arachnidHttpMachine {
            methods PATCH

            patch }

    verify defaultSetup machine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

[<Fact>]
let ``patch disabled ignores patch`` () =

    let machine =
        arachnidHttpMachine {
            methods PATCH

            patch
            patchEnabled false }

    verify defaultSetup machine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK" ]

(* Headers

   Verification that the Accept-Patch header is set appropriately on OPTIONS
   requests. *)

[<Fact>]
let ``accept-patch header set appropriately`` () =

    let setup =
            Request.method_ .= OPTIONS

    (* Patch supported, no Media Types specified. *)

    let machine =
        arachnidHttpMachine {
            methods [ OPTIONS; PATCH ]

            patch }

    verify setup machine [
        Response.statusCode_ => Some 200
        Response.Headers.acceptPatch_ => None ]

    (* Patch supported, Media Types specified. *)

    let machine =
        arachnidHttpMachine {
            methods [ OPTIONS; PATCH ]

            patch
            patchAcceptableMediaTypes MediaType.Json }

    verify setup machine [
        Response.statusCode_ => Some 200
        Response.Headers.acceptPatch_ => Some (AcceptPatch [ MediaType.Json ]) ]

(* Operation *)

[<Fact>]
let ``machine processes operation correctly`` () =

    let setup =
            (Request.method_ .= PATCH)
         *> (Request.Headers.contentLength_ .= Some (ContentLength 0))

    (* Success *)

    let successMachine =
        arachnidHttpMachine {
            methods PATCH

            patch
            patchDoPatch (defaultValue .= Some "Success") }

    verify setup successMachine [
        Response.statusCode_ => Some 200
        Response.reasonPhrase_ => Some "OK"
        defaultValue => Some "Success" ]
